import matplotlib.pyplot as plt
import numpy
import random

from NeuralNetwork import NeuralNetwork

input_nodes = 4096
hidden_nodes = 400
output_nodes = 3

learning_rate = 0.3

network = NeuralNetwork(input_nodes, hidden_nodes, output_nodes, learning_rate)

training_data_file = open("dataset/test_x.csv", 'r')
training_data_list = training_data_file.readlines()
numpy.random.shuffle(training_data_list)
training_data_file.close()

if not training_data_list:
    print('Train dataset is empty')
    exit(0)

for record in training_data_list:
    all_values = record.split(',')
    inputs = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01

    # цільові значення
    targets = numpy.zeros(output_nodes) + 0.01

    targets[int(all_values[0])] = 0.99
    network.train(inputs, targets)
    pass

# todo
logs = []

for record in training_data_list:
        train_values = record.split(',')

        correct_respond = int(train_values[0])

        image_array = numpy.asfarray(train_values[1:]).reshape((64, 64))
        plt.imshow(image_array, cmap='Greys', interpolation='None')

        result = network.query((numpy.asfarray(train_values[1:]) / 255.0 * 0.99) + 0.01)
        respond = numpy.argmax(result)

        if correct_respond == respond:
            logs.append(1)
        else:
            logs.append(0)
        pass

        print('right: ', correct_respond, 'current respond: ', respond)

        pass

logs_array = numpy.asanyarray(logs)
print('efficiency: ', logs_array.sum() / logs_array.size)

