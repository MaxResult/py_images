from Blank import Blank
from NeuralNetwork import NeuralNetwork

blank = Blank('dataset/blanks/2(300).jpg')
cells = blank.get_cells()


input_nodes = 4096
hidden_nodes = 400
output_nodes = 3

learning_rate = 0.3

network = NeuralNetwork(input_nodes, hidden_nodes, output_nodes, learning_rate)