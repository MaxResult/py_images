import scipy.misc
import matplotlib.pyplot as plt
import os
import numpy


def convert(directories, out_file):
    output = open(out_file, "w")

    for directory in directories:
        files = os.listdir(directory)
        target_value = directory.split('/').pop()
        for file in files:
            path = directory + "/" + file
            img_array = scipy.misc.imread(path, flatten=True)

            img_data = img_array.reshape(4096)
            img_data = (255.0 - img_data)

            img_array = numpy.asfarray(img_data).reshape((64, 64))

            plt.imshow(img_array, cmap='Greys', interpolation='None')

            # img_data = img_array.reshape(4096)

            # img_data = (img_data / 255.0 * 0.99) + 0.01

            output.write(target_value + "," + ",".join(str(pix) for pix in img_data) + "\n")
            pass
    pass

    output.close()

    pass


directories = ['dataset/images/300/0', 'dataset/images/300/1', 'dataset/images/300/2']

convert(directories, 'dataset/test_x.csv')

print('Convert images to dataset was finished')
