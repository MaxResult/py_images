import numpy
# бібліотека для графічного відображення масивів
import matplotlib.pyplot as plt

from NeuralNetwork import NeuralNetwork

input_nodes = 784
hidden_nodes = 100
output_nodes = 10

learning_rate = 0.3

network = NeuralNetwork(input_nodes, hidden_nodes, output_nodes, learning_rate)


training_data_file = open("../dataset/mnist_train_100.csv", 'r')
training_data_list = training_data_file.readlines()
training_data_file.close()

for record in training_data_list:
    all_values = record.split(',')
    inputs = (numpy.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
    # создать целевые выходные значения (все равны 0,01, за исключением
    # желаемого маркерного значения, равного 0,99)
    targets = numpy.zeros(output_nodes) + 0.01

    # all_values[0] - целевое маркерное значение для данной записи
    targets[int(all_values[0])] = 0.99
    network.train(inputs, targets)
    pass


test_data_file = open("../dataset/mnist_test_10.csv", 'r')
test_data_list = test_data_file.readlines()
test_data_file.close()

logs = []

epochs = 2
for e in range(epochs):
    for record in test_data_list:
        train_values = record.split(',')

        correct_respond = int(train_values[0])

        # image_array = numpy.asfarray(train_values[1:]).reshape((28, 28))
        # plt.imshow(image_array, cmap='Greys', interpolation='None')

        result = network.query((numpy.asfarray(train_values[1:]) / 255.0 * 0.99) + 0.01)
        respond = numpy.argmax(result)

        if correct_respond == respond:
            logs.append(1)
        else:
            logs.append(0)
        pass

        print('right: ', correct_respond, 'current respond: ', respond)

        pass
    pass

logs_array = numpy.asanyarray(logs)
print('efficiency: ', logs_array.sum() / logs_array.size)


# all_values = training_data_list[0].split(',')
# image_array = numpy.asfarray(all_values[1:]).reshape((28, 28))
# plt.imshow(image_array, cmap='Greys', interpolation='None')
# plt.show()

# Створення цільової матриці
# target_output_nodes = 10  # кількість вихідних вузлів
# targets = numpy.zeros(target_output_nodes) + 0.01
# targets[int(all_values[0])] = 0.99

# r = n.query([1.0, 0.5, -1.5])

