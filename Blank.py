from PIL import Image, ImageDraw, ImageFont
import math


class Blank:
    def __init__(self, path):
        self.image = Image.open(path)

        self.width, self.height = self.image.size

        # якщо зображення горизонтальне, а не вертикальне - то повернути
        if self.width > self.height:
            self.image.rotate(90)
            self.width, self.height = self.height, self.width

        # кількість пікселів в 1% зображення
        self.width_percent = self.width / 100
        self.height_percent = self.height / 100

        self.pixels = list(self.image.getdata())
        pass

    def __del__(self):
        self.image.close()

    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.misc.imread.html
    def translate_color(self, pixel):
        return pixel[0] * 299 / 1000 + pixel[1] * 587 / 1000 + pixel[2] * 114 / 1000

    # Get area where can be marker (get by black pixel)
    # begin of coordinate (x, y) - top left side
    #
    #  +---------+
    #  |#       @|
    #  |         |
    #  |         |
    #  |         |
    #  |#       #|
    #  +---------+
    #
    # hp_start - height percent start
    # hp_stop  - height percent stop
    # wp_start - width percent start
    # wp_stop  - width percent stop
    def get_marker(self, hp_start, hp_stop, wp_start, wp_stop):
        marker = []
        h_list = [i for i in range(round(hp_start * self.height_percent), round(hp_stop * self.height_percent), 4)]
        w_list = [i for i in range(round(wp_start * self.width_percent), round(wp_stop * self.width_percent), 4)]

        draw = ImageDraw.Draw(self.image)

        for w in w_list:
            row = []
            for h in h_list:
                pixel = self.image.getpixel((w, h))

                translate_color = self.translate_color(pixel)
                # draw.point((w, h), (0, 255, 0))

                if translate_color <= 150:
                    # draw.point((w, h), (255, 0, 0))
                    row.append((w, h))

            if len(row) != 0:
                marker.append(row)

        # todo
        first_row = marker[0]
        vertex_1 = first_row[0]
        vertex_3 = first_row.pop()

        last_row = marker.pop()
        vertex_4 = last_row[0]
        vertex_2 = last_row.pop()

        x, y = self.intersection_of_lines(vertex_1, vertex_2, vertex_3, vertex_4)
        draw.point((x, y), (255, 0, 0))
        draw.point((x+1, y+1), (255, 0, 0))
        draw.point((x+2, y+2), (255, 0, 0))
        draw.point((x+3, y+3), (255, 0, 0))
        draw.point((x+4, y+4), (255, 0, 0))
        draw.point((x+5, y+5), (255, 0, 0))
        self.image.save("test.jpg")

        return x, y

    # get coordinate of intersection of 2 lines: v1v2 and v3v4
    def intersection_of_lines(self, v1, v2, v3, v4):
        x = -((v1[0] * v2[1] - v2[0] * v1[1]) * (v4[0] - v3[0]) - (v3[0] * v4[1] - v4[0] * v3[1]) * (v2[0] - v1[0])) / (
            (v1[1] - v2[1]) * (v4[0] - v3[0]) - (v3[1] - v4[1]) * (v2[0] - v1[0]))
        y = ((v3[1] - v4[1]) * (-x) - (v3[0] * v4[1] - v4[0] * v3[1])) / (v4[0] - v3[0])

        return x, y

    def check(self):

        pass

    def get_cells(self):

        left_top_coordinate = self.get_marker(2, 12, 1, 5)
        left_bottom_coordinate = self.get_marker(88, 98, 1, 5)
        right_bottom_coordinate = self.get_marker(88, 98, 92, 98)

        # righ_top_coordinate = self.get_marker(2, 12, 92, 98)

        return []




