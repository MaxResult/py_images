import matplotlib.pyplot as plt
import numpy


training_data_file = open("../dataset/test_x.csv", 'r')
training_data_list = training_data_file.readlines()
training_data_file.close()

for record in training_data_list:
    train_values = record.split(',')

    a = numpy.asfarray(train_values)

    image_array = numpy.asfarray(train_values).reshape((64, 64))
    plt.imshow(image_array, cmap='Greys', interpolation='None')

    pass

