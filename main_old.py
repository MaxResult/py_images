from PIL import Image, ImageDraw, ImageFont
import math

# image = Image.open('images/blank1.jpg')
image = Image.open('dataset/blanks/2(300).jpeg')

width, height = image.size

# якщо зображення горизонтальне, а не вертикальне - то повернути
if width > height:
    image.rotate(90)
    width, height = height, width

# кількість пікселів в 1% зображення
width_percent = width / 100
height_percent = height / 100

pixels = list(image.getdata())


def get_sum(p):
    return math.sqrt(p[0]*p[0] + p[1]*p[1] + p[1]*p[1])


# отримання масиву пікселів маркера (сут. маркер - todo)
def get_marker(hp_start, hp_stop, wp_start, wp_stop):
    marker = []
    h_list = [i / 2 for i in range(hp_start*2, hp_stop*2, 1)]
    w_list = [i / 2 for i in range(wp_start*2, wp_stop*2, 1)]
    for h in h_list:
        row = []
        for w in w_list:
            x = round(w * width_percent)
            y = round(h * height_percent)
            pixel = image.getpixel((x, y))

            sum = get_sum(pixel)
            if sum <= 225:
                row.append((x, y))

        if len(row) != 0:
            marker.append(row)
    return marker


# перевірка чи масив пікселів є маркером (сут. маркер - todo)
def check_marker(marker):
    right_rows = 0
    possible_mistakes = 0
    for row in marker:
        if len(row) == 2 or len(row) == 3:
            right_rows += 1
        elif len(row) == 1:
            possible_mistakes += 1

    if 9 <= right_rows <= 14 and possible_mistakes <= 3:
        return True
    else:
        return False


# відстань між двома точками
def get_distance(a, b):
    return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)


# отримання першого пікселя маркера (сут. маркер - todo)
def get_first_pixel(marker):
    return marker[0][0]


def get_last_pixel(marker):
    length = len(marker[0])
    return marker[0][length-1]


def get_angle_by_sin(sin):
    # math.asin(sin) / math.pi * 180
    return math.asin(sin)


def get_offset_by_x(x, y, angle):
    return x * math.cos(angle) - y * math.sin(angle)


def get_offset_by_y(x, y, angle):
    return x * math.sin(angle) + y * math.cos(angle)


#
def get_offset_x_by_marker(x, y, angle_1, angle_2):
    x = get_offset_by_x(x, y, angle_1)
    return get_offset_by_x(x, y, angle_2)


#
def get_offset_y_by_marker(x, y, angle_1, angle_2):
    y = get_offset_by_y(x, y, angle_1)
    return get_offset_by_y(x, y, angle_2)


# обробка колонки з відповідями
def column_process():
    lp_first = get_last_pixel(first_marker)
    # lp_second = get_last_pixel(second_marker)

    # координати чотирьох точок, для розпізнання комірки відповіді
    x1 = lp_first[0] + 2.9 * width_percent
    y1 = lp_first[1]
    x2 = lp_first[0] + 5.4 * width_percent
    y2 = lp_first[1]
    x3 = lp_first[0] + 5.4 * width_percent
    y3 = lp_first[1] + 2 * height_percent
    x4 = lp_first[0] + 3 * width_percent
    y4 = lp_first[1] + 2 * height_percent

    # coords = [[x1, y1], [x2, y2], [x3, y3], [x4, y4]]

    questions = []
    # кількість комірок
    # box_list = range(1, 5 * 3, 1)
    # кількість стрічок з комірками
    # row_list = range(1, 17 * 4, 1)

    box_list = [i * 3 for i in range(0, 5, 1)]
    row_list = [i * 5.4 for i in range(0, 16, 1)]

    for x in row_list:
        row = []
        for y in box_list:
            boxes = []
            box_pixels = []
            # for coord in coords:
            # роблю здвиг пікселів для перевірки в середину
            x_1 = x1 + x * width_percent
            # x_1 = x1 + x * width_percent + width_percent * 0.2
            y_1 = y1 + y * height_percent + height_percent * 0.2
            x_1 = get_offset_x_by_marker(x_1, y_1, first_second_angle, second_third_angle)
            y_1 = get_offset_y_by_marker(-x_1, y_1, first_second_angle, second_third_angle)

            x_2 = x2 + x * width_percent - width_percent * 0.2
            y_2 = y2 + y * height_percent + height_percent * 0.2
            x_2 = get_offset_x_by_marker(x_2, y_2, first_second_angle, second_third_angle)
            y_2 = get_offset_y_by_marker(-x_2, y_2, first_second_angle, second_third_angle)

            x_3 = x3 + x * width_percent - width_percent * 0.2
            y_3 = y3 + y * height_percent - height_percent * 0.2
            x_3 = get_offset_x_by_marker(x_3, y_3, first_second_angle, second_third_angle)
            y_3 = get_offset_y_by_marker(x_3, y_3, first_second_angle, -second_third_angle)

            x_4 = x4 + x * width_percent + width_percent * 0.2
            y_4 = y4 + y * height_percent - height_percent * 0.2
            x_4 = get_offset_x_by_marker(x_4, y_4, first_second_angle, second_third_angle)
            y_4 = get_offset_y_by_marker(x_4, y_4, first_second_angle, -second_third_angle)

            box_pixels.append(image.getpixel((x_1, y_1)))
            box_pixels.append(image.getpixel((x_2, y_2)))
            box_pixels.append(image.getpixel((x_3, y_3)))
            box_pixels.append(image.getpixel((x_4, y_4)))
            # todo було би круто знаходити точку перетину (центр) і перевіряти ще її

            for box_pixel in box_pixels:
                sum = get_sum(box_pixel)
                if sum <= 378:
                    boxes.append(True)
            if 2 <= len(boxes) <= 4:
                row.append(True)
            else:
                row.append(False)
        questions.append(row)

    return questions


# обробка стрічки колонки
def row_process():

    return 1


# отримання маркерів
first_marker = get_marker(2, 12, 1, 6)
second_marker = get_marker(2, 12, 92, 98)
third_marker = get_marker(88, 98, 92, 98)

# перевірка маркерів
first_marker_valid = check_marker(first_marker)
second_marker_valid = check_marker(second_marker)
third_marker_valid = check_marker(third_marker)

if first_marker_valid is False or second_marker_valid is False or third_marker is False:
    fourth_marker = get_marker(88, 98, 1, 6)
    # todo - визначити правильну орієнтацію
else:
    # перші пікселі маркерів
    fp_first = get_first_pixel(first_marker)
    fp_second = get_first_pixel(second_marker)
    fp_third = get_first_pixel(third_marker)

    # кут між першим і другим маркером
    B = (fp_second[0], fp_first[1])
    c = get_distance(fp_first, fp_second)
    a = get_distance(B, fp_second)
    sin_a = a / c
    first_second_angle = get_angle_by_sin(sin_a)
    #
    if fp_first[1] > fp_second[1]:
        first_second_angle = -first_second_angle

    # кут між другим і третім маркером
    B = (fp_second[0], fp_third[1])
    c = get_distance(fp_second, fp_third)
    a = get_distance(B, fp_third)
    sin_a = a / c
    second_third_angle = get_angle_by_sin(sin_a)
    #
    if fp_second[0] < fp_third[0]:
        second_third_angle = -second_third_angle

    last = get_last_pixel(first_marker)

    # first_column = column_process()
    # print(first_column)
    # todo

