import numpy
import scipy.special


class NeuralNetwork:
    # ініціалізувати нейронну мережу
    def __init__(self, input_nodes, hidden_nodes, output_nodes, learning_rate):
        # задати кількість вузлів у вхідному, прихованому і вихідному шарі
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.output_nodes = output_nodes

        # Матриці вагових коефіцієнтів зв'язків wih і who.
        # Вагові коефіцієнти зв'язків між вузлом i та вузлом j
        # наступного шару позначені як w__i__j:
        # w11 w21
        # w12 w22 і т.д.
        self.wih = numpy.random.normal(0.0, pow(self.hidden_nodes, -0.5), (self.hidden_nodes, self.input_nodes))
        self.who = numpy.random.normal(0.0, pow(self.output_nodes, -0.5), (self.output_nodes, self.hidden_nodes))

        # коефіцієнт навчання
        self.lr = learning_rate
        # використання сигмоіди в якості функції активації
        self.activation_function = lambda x: scipy.special.expit(x)
        pass

    # тренування нейронної мережі
    def train(self, inputs_list, targets_list):
        # перетворити список вхідних значень в двовимірний масив
        inputs = numpy.array(inputs_list, ndmin=2).T
        targets = numpy.array(targets_list, ndmin=2).T

        # розрахувати вхідні сигнали для прихованого шару
        hidden_inputs = numpy.dot(self.wih, inputs)

        # розрахувати вихідні сигнали для прихованого шару
        hidden_outputs = self.activation_function(hidden_inputs)

        # розрахувати вхідні сигнали для вихідного шару
        final_inputs = numpy.dot(self.who, hidden_outputs)

        # розрахувати вихідні сигнали для вихідного шару
        final_outputs = self.activation_function(final_inputs)

        # помилка = цільове значення - фактичне значення
        output_errors = targets - final_outputs

        # Помилки прихованого шару - це помилки output_errors,
        # Розподілені пропорційно ваговим коефіцієнтам зв'язків
        # і рекомбіновані на прихованих вузлах
        hidden_errors = numpy.dot(self.who.T, output_errors)

        self.who += self.lr * numpy.dot((output_errors * final_outputs *
                                         (1.0 - final_outputs)), numpy.transpose(hidden_outputs))

        # оновити вагові коефіцієнти зв'язків між прихованим і вихідним шарами
        self.who += self.lr * numpy.dot((output_errors * final_outputs *
                                         (1.0 - final_outputs)), numpy.transpose(hidden_outputs))

        # оновити вагові коефіцієнти зв'язків між вхідним і прихованим шарами
        self.wih += self.lr * numpy.dot((hidden_errors * hidden_outputs *
                                         (1.0 - hidden_outputs)), numpy.transpose(inputs))
        pass

    # опитування нейронної мережі
    def query(self, inputs_list):
        # Перетворити список вхідних значень в двовимірний масив
        inputs = numpy.array(inputs_list, ndmin=2).T

        # розрахувати вхідні сигнали для прихованого шару
        hidden_inputs = numpy.dot(self.wih, inputs)

        # розрахувати вихідні сигнали для прихованого шару
        hidden__outputs = self.activation_function(hidden_inputs)
        # розрахувати вхідні сигнали для вихідного шару
        final_inputs = numpy.dot(self.who, hidden__outputs)
        # розрахувати вихідні сигнали для вихідного шару
        final_outputs = self.activation_function(final_inputs)
        return final_outputs
